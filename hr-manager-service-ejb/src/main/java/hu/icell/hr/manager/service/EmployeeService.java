package hu.icell.hr.manager.service;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;

import hu.icell.hr.manager.dao.EmployeeDAO;
import hu.icell.hr.manager.model.Employee;


@Local(value = {LocalEmployeeService.class})
@Remote(value = {RemoteEmployeeService.class})
@Stateless
public class EmployeeService implements RemoteEmployeeService, LocalEmployeeService {

	
	@Inject
	private EmployeeDAO dao;
	
	public List<Employee> getAllEmployees() {
		return dao.getAll();
	}

}
